﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Bloc_de_Notas
{
    static class Buscador
    {

        /**
         * Usando una cadena como referencia base, devuelve el número total
         * de ocurrencias de "valor" en la misma.
         */
        public static int GetNumeroCoincidencias(this String textoBase, String valor)
        {
            if (String.IsNullOrEmpty(textoBase) || String.IsNullOrEmpty(valor))
                return 0;

            return Regex.Matches(textoBase, valor).Count;
        }

        /**
         * Usando una cadena de referencia, busca la primera ocurrencia de "valorBuscar"
         * y la reemplaza por "valorReemplazar".
         */
        public static String ReemplazarPrimeraCoincidencia(this String textoBase, String valorBuscar, String valorReemplazar)
        {
            Regex rx = new Regex(Regex.Escape(valorBuscar));

            return rx.Replace(textoBase, valorReemplazar, 1);
        }

        /**
         * Usando un RichTextBox como elemento base y dada una cadena,
         * busca las ocurrencias de la misma y cambia el color de ellas
         * para mostrarlas visualmente al usuario.
         * */
        public static void ResaltarTexto(this RichTextBox campoTexto, string texto)
        {

            campoTexto.SelectAll();
            campoTexto.SelectionColor = Color.Black;
            campoTexto.DeselectAll();

            if (!texto.Equals(String.Empty))
            {
                int comienzoSeleccion = campoTexto.SelectionStart;
                int indiceInicio = 0;
                int indice = 0;

                while ((indice = campoTexto.Text.IndexOf(texto, indiceInicio)) != -1)
                {

                    campoTexto.Select(indice, texto.Length);
                    campoTexto.SelectionColor = Color.FromArgb(26, 194, 56);

                    indiceInicio = indice + texto.Length;
                }

            }
        }


    }

}
