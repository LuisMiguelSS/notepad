﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Bloc_de_Notas
{
    public class GestorArchivos
    {
        //
        // Constantes
        //
        public static string RUTA_INICIAL_DEFECTO = Environment.SpecialFolder.Desktop.ToString();
        public const string NOMBRE_ARCHIVO_DEFECTO = "Mi documento";
        public const string EXTENSIONES_PERMITIDAS_ESCRITURA = "Archivo de texto (*.txt)|*.txt|Archivo de texto enriquecido (*.rtf)|*.rtf";
        public const string EXTENSIONES_PERMITIDAS_LECTURA = "Archivos de texto simple (*.txt y *.rtf)|*.txt;*.rtf|" + EXTENSIONES_PERMITIDAS_ESCRITURA;

        //
        // Atributos
        //
        private List<String> archivosProcesados;
        private bool archivoGuardado;

        //
        // Constructores
        //
        public GestorArchivos() : this(new List<String>(), false) {}
        public GestorArchivos(List<String> archivosProcesados, bool archivoGuardado) {
            this.archivosProcesados = archivosProcesados;
            this.archivoGuardado = archivoGuardado;
        }

        //
        // Getters
        //
        public List<String> getArchivosProcesados() { return archivosProcesados; }
        public bool isArchivoGuardado() { return archivoGuardado; }
        public bool isArchivoProcesado(String archivo)
        {
            return archivosProcesados.Contains(archivo);
        }
        public String getUltimoArchivoProcesado()
        {
            if (hayArchivosProcesados())
                return archivosProcesados[archivosProcesados.Count - 1];
            else
                return String.Empty;
        }

        //
        // Setters
        //
        public void setArchivosProcesados(List<String> nuevaListaArchivos)
        {
            this.archivosProcesados = nuevaListaArchivos;
        }
        public void setArchivoGuardado(bool estado)
        {
            this.archivoGuardado = estado;
        }

        //
        // Otros métodos
        //
        public bool hayArchivosProcesados()
        {
            return archivosProcesados.Count > 0 ? true : false;
        }

        /**
         * Dada la ruta hacia un archivo, comprueba si éste
         * ya se ha abierto con anterioridad o no.
         */
        public bool anadirArchivo(String archivo)
        {
            if (!isArchivoProcesado(archivo))
            {
                this.archivosProcesados.Add(archivo);
                return true;

            }
            
            return false;
        }

        /**
         * Dados un RichTextBox y la ruta hacia un archivo,
         * obtiene su contenido y lo muestra en el campo de texto
         */
        public void AbrirArchivo(RichTextBox campoTexto, string archivo)
        {
            // Comprobar extensiones
            if (Path.GetExtension(archivo).ToLower().Equals(".txt"))
                campoTexto.Text = File.ReadAllText(archivo, Encoding.UTF8);
            else if (Path.GetExtension(archivo).ToLower().Equals(".rtf"))
                campoTexto.LoadFile(archivo, RichTextBoxStreamType.RichText);


            archivoGuardado = true;
        }

        /**
         * Devuelve un cuadro de diálogo de guardado
         * con filtros ya definidos. A diferencia de "GetDialogoGuardarComo",
         * esta función permite la opción de "Todos los archivos TXT y RTF"
         * en el desplegable.
         */
        public SaveFileDialog GetDialogoGuardar()
        {
            SaveFileDialog dialogo = new SaveFileDialog();
                dialogo.InitialDirectory = RUTA_INICIAL_DEFECTO;
                dialogo.FileName = NOMBRE_ARCHIVO_DEFECTO;
                dialogo.Filter = EXTENSIONES_PERMITIDAS_LECTURA;

            return dialogo;
        }

        /**
         * Devuelve un cuadro de diálogo de guardado
         * ya definido con los filtros establecidos.
         */
        public SaveFileDialog GetDialogoGuardarComo()
        {
            SaveFileDialog dialogo = new SaveFileDialog();
            dialogo.InitialDirectory = RUTA_INICIAL_DEFECTO;
            dialogo.FileName = NOMBRE_ARCHIVO_DEFECTO;
            dialogo.Filter = EXTENSIONES_PERMITIDAS_ESCRITURA;

            return dialogo;
        }

        /**
         * Devuelve un cuadro de diálogo de elección de archivos
         * ya definido con los filtros establecidos.
         */
        public OpenFileDialog GetDialogoElegirArchivo()
        {
            OpenFileDialog dialogo = new OpenFileDialog();
                dialogo.Filter = EXTENSIONES_PERMITIDAS_LECTURA;
                dialogo.InitialDirectory = GestorArchivos.RUTA_INICIAL_DEFECTO;

            return dialogo;
        }

    }
}
