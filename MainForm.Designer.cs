﻿using System.Windows.Forms;

namespace Bloc_de_Notas
{
    partial class VentanaPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaPrincipal));
            this.barraMenu = new System.Windows.Forms.MenuStrip();
            this.toolstripmenu_item_Archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnNuevo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnAbrir = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.menuBtnGuardar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnGuardarComo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuBtnSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.toolstripmenu_item_Editar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnDeshacer = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnRehacer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuBtnCortar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnCopiar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnPegar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuBtnBuscar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnBuscarReemplazar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnSoloBuscar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnFuente = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnColor = new System.Windows.Forms.ToolStripMenuItem();
            this.barraHerramientas = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnAbrir = new System.Windows.Forms.ToolStripButton();
            this.btnGuardar = new System.Windows.Forms.ToolStripButton();
            this.btnGuardarComo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDeshacer = new System.Windows.Forms.ToolStripButton();
            this.btnRehacer = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCortar = new System.Windows.Forms.ToolStripButton();
            this.btnCopiar = new System.Windows.Forms.ToolStripButton();
            this.btnPegar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btnFuente = new System.Windows.Forms.ToolStripButton();
            this.btnColor = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.btnBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnBuscarReemplazar = new System.Windows.Forms.ToolStripButton();
            this.barraEstado_inferior = new System.Windows.Forms.StatusStrip();
            this.status_label_posicion = new System.Windows.Forms.ToolStripStatusLabel();
            this.label_alineacionDerecha = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.label_Zoom = new System.Windows.Forms.ToolStripStatusLabel();
            this.label_NumeroCoincidenciasBusqueda = new System.Windows.Forms.ToolStripStatusLabel();
            this.menu_clickDerecho = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenu_item_Cortar = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu_item_Copiar = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu_item_Pegar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.contextMenuFuente = new System.Windows.Forms.ToolStripMenuItem();
            this.dialog_fuente = new System.Windows.Forms.FontDialog();
            this.dialog_color = new System.Windows.Forms.ColorDialog();
            this.layoutTablaPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.inputCuadroTexto = new System.Windows.Forms.RichTextBox();
            this.panelBuscarReemplazar = new System.Windows.Forms.TableLayoutPanel();
            this.btnReemplazarSiguiente = new System.Windows.Forms.Button();
            this.labelBuscar = new System.Windows.Forms.Label();
            this.labelReemplazar = new System.Windows.Forms.Label();
            this.inputBuscar = new System.Windows.Forms.TextBox();
            this.btnBuscarTodos = new System.Windows.Forms.Button();
            this.inputReemplazar = new System.Windows.Forms.TextBox();
            this.lblCierraBuscarReemplazar = new System.Windows.Forms.Label();
            this.btnReemplazarTodos = new System.Windows.Forms.Button();
            this.barraMenu.SuspendLayout();
            this.barraHerramientas.SuspendLayout();
            this.barraEstado_inferior.SuspendLayout();
            this.menu_clickDerecho.SuspendLayout();
            this.layoutTablaPrincipal.SuspendLayout();
            this.panelBuscarReemplazar.SuspendLayout();
            this.SuspendLayout();
            // 
            // barraMenu
            // 
            this.barraMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolstripmenu_item_Archivo,
            this.toolstripmenu_item_Editar});
            this.barraMenu.Location = new System.Drawing.Point(0, 0);
            this.barraMenu.Name = "barraMenu";
            this.barraMenu.Size = new System.Drawing.Size(801, 24);
            this.barraMenu.TabIndex = 0;
            this.barraMenu.Text = "menuStrip1";
            // 
            // toolstripmenu_item_Archivo
            // 
            this.toolstripmenu_item_Archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBtnNuevo,
            this.menuBtnAbrir,
            this.toolStripSeparator,
            this.menuBtnGuardar,
            this.menuBtnGuardarComo,
            this.toolStripSeparator1,
            this.menuBtnSalir});
            this.toolstripmenu_item_Archivo.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolstripmenu_item_Archivo.Name = "toolstripmenu_item_Archivo";
            this.toolstripmenu_item_Archivo.Size = new System.Drawing.Size(60, 20);
            this.toolstripmenu_item_Archivo.Text = "&Archivo";
            // 
            // menuBtnNuevo
            // 
            this.menuBtnNuevo.Image = global::Bloc_de_Notas.Properties.Resources.NewFile_16x;
            this.menuBtnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBtnNuevo.Name = "menuBtnNuevo";
            this.menuBtnNuevo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuBtnNuevo.Size = new System.Drawing.Size(236, 22);
            this.menuBtnNuevo.Text = "&Nuevo";
            this.menuBtnNuevo.Click += new System.EventHandler(this.menuBtnNuevo_Click);
            // 
            // menuBtnAbrir
            // 
            this.menuBtnAbrir.Image = global::Bloc_de_Notas.Properties.Resources.OpenFile_16x;
            this.menuBtnAbrir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBtnAbrir.Name = "menuBtnAbrir";
            this.menuBtnAbrir.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuBtnAbrir.Size = new System.Drawing.Size(236, 22);
            this.menuBtnAbrir.Text = "&Abrir";
            this.menuBtnAbrir.Click += new System.EventHandler(this.menuBtnAbrir_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(233, 6);
            // 
            // menuBtnGuardar
            // 
            this.menuBtnGuardar.Image = global::Bloc_de_Notas.Properties.Resources.Save_16x;
            this.menuBtnGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBtnGuardar.Name = "menuBtnGuardar";
            this.menuBtnGuardar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuBtnGuardar.Size = new System.Drawing.Size(236, 22);
            this.menuBtnGuardar.Text = "&Guardar";
            this.menuBtnGuardar.Click += new System.EventHandler(this.menuBtnGuardar_Click);
            // 
            // menuBtnGuardarComo
            // 
            this.menuBtnGuardarComo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuBtnGuardarComo.Image = ((System.Drawing.Image)(resources.GetObject("menuBtnGuardarComo.Image")));
            this.menuBtnGuardarComo.Name = "menuBtnGuardarComo";
            this.menuBtnGuardarComo.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.menuBtnGuardarComo.Size = new System.Drawing.Size(236, 22);
            this.menuBtnGuardarComo.Text = "G&uardar como";
            this.menuBtnGuardarComo.Click += new System.EventHandler(this.menuBtnGuardarComo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(233, 6);
            // 
            // menuBtnSalir
            // 
            this.menuBtnSalir.Image = global::Bloc_de_Notas.Properties.Resources.Exit_16x;
            this.menuBtnSalir.Name = "menuBtnSalir";
            this.menuBtnSalir.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.menuBtnSalir.Size = new System.Drawing.Size(236, 22);
            this.menuBtnSalir.Text = "Salir";
            this.menuBtnSalir.Click += new System.EventHandler(this.menuBtnSalir_Click);
            // 
            // toolstripmenu_item_Editar
            // 
            this.toolstripmenu_item_Editar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBtnDeshacer,
            this.menuBtnRehacer,
            this.toolStripSeparator3,
            this.menuBtnCortar,
            this.menuBtnCopiar,
            this.menuBtnPegar,
            this.toolStripSeparator4,
            this.menuBtnBuscar,
            this.menuBtnFuente,
            this.menuBtnColor});
            this.toolstripmenu_item_Editar.Name = "toolstripmenu_item_Editar";
            this.toolstripmenu_item_Editar.Size = new System.Drawing.Size(49, 20);
            this.toolstripmenu_item_Editar.Text = "&Editar";
            // 
            // menuBtnDeshacer
            // 
            this.menuBtnDeshacer.Image = global::Bloc_de_Notas.Properties.Resources.Undo_16x;
            this.menuBtnDeshacer.Name = "menuBtnDeshacer";
            this.menuBtnDeshacer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.menuBtnDeshacer.Size = new System.Drawing.Size(163, 22);
            this.menuBtnDeshacer.Text = "&Deshacer";
            this.menuBtnDeshacer.Click += new System.EventHandler(this.menuBtnDeshacer_Click);
            // 
            // menuBtnRehacer
            // 
            this.menuBtnRehacer.Image = global::Bloc_de_Notas.Properties.Resources.Redo_16x;
            this.menuBtnRehacer.Name = "menuBtnRehacer";
            this.menuBtnRehacer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.menuBtnRehacer.Size = new System.Drawing.Size(163, 22);
            this.menuBtnRehacer.Text = "&Rehacer";
            this.menuBtnRehacer.Click += new System.EventHandler(this.menuBtnRehacer_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(160, 6);
            // 
            // menuBtnCortar
            // 
            this.menuBtnCortar.Image = global::Bloc_de_Notas.Properties.Resources.Cut_16x;
            this.menuBtnCortar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBtnCortar.Name = "menuBtnCortar";
            this.menuBtnCortar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.menuBtnCortar.Size = new System.Drawing.Size(163, 22);
            this.menuBtnCortar.Text = "Cor&tar";
            this.menuBtnCortar.Click += new System.EventHandler(this.menuBtnCortar_Click);
            // 
            // menuBtnCopiar
            // 
            this.menuBtnCopiar.Image = global::Bloc_de_Notas.Properties.Resources.Copy_16x;
            this.menuBtnCopiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBtnCopiar.Name = "menuBtnCopiar";
            this.menuBtnCopiar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.menuBtnCopiar.Size = new System.Drawing.Size(163, 22);
            this.menuBtnCopiar.Text = "&Copiar";
            this.menuBtnCopiar.Click += new System.EventHandler(this.menuBtnCopiar_Click);
            // 
            // menuBtnPegar
            // 
            this.menuBtnPegar.Image = global::Bloc_de_Notas.Properties.Resources.Paste_16x;
            this.menuBtnPegar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBtnPegar.Name = "menuBtnPegar";
            this.menuBtnPegar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.menuBtnPegar.Size = new System.Drawing.Size(163, 22);
            this.menuBtnPegar.Text = "&Pegar";
            this.menuBtnPegar.Click += new System.EventHandler(this.menuBtnPegar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(160, 6);
            // 
            // menuBtnBuscar
            // 
            this.menuBtnBuscar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBtnBuscarReemplazar,
            this.menuBtnSoloBuscar});
            this.menuBtnBuscar.Image = global::Bloc_de_Notas.Properties.Resources.Search_16xMD;
            this.menuBtnBuscar.Name = "menuBtnBuscar";
            this.menuBtnBuscar.Size = new System.Drawing.Size(163, 22);
            this.menuBtnBuscar.Text = "Buscar";
            this.menuBtnBuscar.Click += new System.EventHandler(this.menuBtnBuscar_Click);
            // 
            // menuBtnBuscarReemplazar
            // 
            this.menuBtnBuscarReemplazar.Image = global::Bloc_de_Notas.Properties.Resources.ReplaceAll_16x;
            this.menuBtnBuscarReemplazar.Name = "menuBtnBuscarReemplazar";
            this.menuBtnBuscarReemplazar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.menuBtnBuscarReemplazar.Size = new System.Drawing.Size(229, 22);
            this.menuBtnBuscarReemplazar.Text = "Buscar y reemplazar...";
            this.menuBtnBuscarReemplazar.Click += new System.EventHandler(this.menuBtnBuscarReemplazar_Click);
            // 
            // menuBtnSoloBuscar
            // 
            this.menuBtnSoloBuscar.Name = "menuBtnSoloBuscar";
            this.menuBtnSoloBuscar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.menuBtnSoloBuscar.Size = new System.Drawing.Size(229, 22);
            this.menuBtnSoloBuscar.Text = "Sólo buscar...";
            this.menuBtnSoloBuscar.Click += new System.EventHandler(this.menuBtnSoloBuscar_Click);
            // 
            // menuBtnFuente
            // 
            this.menuBtnFuente.Image = global::Bloc_de_Notas.Properties.Resources.FontColor_16x;
            this.menuBtnFuente.Name = "menuBtnFuente";
            this.menuBtnFuente.Size = new System.Drawing.Size(163, 22);
            this.menuBtnFuente.Text = "Fuente...";
            this.menuBtnFuente.Click += new System.EventHandler(this.herramientasFuente_Click);
            // 
            // menuBtnColor
            // 
            this.menuBtnColor.Image = global::Bloc_de_Notas.Properties.Resources.ColorWheel_16x;
            this.menuBtnColor.Name = "menuBtnColor";
            this.menuBtnColor.Size = new System.Drawing.Size(163, 22);
            this.menuBtnColor.Text = "Color...";
            this.menuBtnColor.Click += new System.EventHandler(this.menuBtnColor_Click);
            // 
            // barraHerramientas
            // 
            this.barraHerramientas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnAbrir,
            this.btnGuardar,
            this.btnGuardarComo,
            this.toolStripSeparator2,
            this.btnDeshacer,
            this.btnRehacer,
            this.toolStripSeparator5,
            this.btnCortar,
            this.btnCopiar,
            this.btnPegar,
            this.toolStripSeparator6,
            this.btnFuente,
            this.btnColor,
            this.toolStripSeparator7,
            this.btnBuscar,
            this.btnBuscarReemplazar});
            this.barraHerramientas.Location = new System.Drawing.Point(0, 24);
            this.barraHerramientas.Name = "barraHerramientas";
            this.barraHerramientas.Size = new System.Drawing.Size(801, 25);
            this.barraHerramientas.TabIndex = 1;
            this.barraHerramientas.Text = "toolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNuevo.Image = global::Bloc_de_Notas.Properties.Resources.NewFile_16x;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(23, 22);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnAbrir
            // 
            this.btnAbrir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAbrir.Image = global::Bloc_de_Notas.Properties.Resources.OpenFile_16x;
            this.btnAbrir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(23, 22);
            this.btnAbrir.Text = "&Abrir";
            this.btnAbrir.Click += new System.EventHandler(this.btnAbrir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnGuardar.Image = global::Bloc_de_Notas.Properties.Resources.Save_16x;
            this.btnGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(23, 22);
            this.btnGuardar.Text = "&Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnGuardarComo
            // 
            this.btnGuardarComo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnGuardarComo.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarComo.Image")));
            this.btnGuardarComo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGuardarComo.Name = "btnGuardarComo";
            this.btnGuardarComo.Size = new System.Drawing.Size(23, 22);
            this.btnGuardarComo.Text = "Guardar como...";
            this.btnGuardarComo.Click += new System.EventHandler(this.btnGuardarComo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDeshacer
            // 
            this.btnDeshacer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeshacer.Image = global::Bloc_de_Notas.Properties.Resources.Undo_16x;
            this.btnDeshacer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeshacer.Name = "btnDeshacer";
            this.btnDeshacer.Size = new System.Drawing.Size(23, 22);
            this.btnDeshacer.Text = "Deshacer";
            this.btnDeshacer.Click += new System.EventHandler(this.btnDeshacer_Click);
            // 
            // btnRehacer
            // 
            this.btnRehacer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRehacer.Image = global::Bloc_de_Notas.Properties.Resources.Redo_16x;
            this.btnRehacer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRehacer.Name = "btnRehacer";
            this.btnRehacer.Size = new System.Drawing.Size(23, 22);
            this.btnRehacer.Text = "Rehacer";
            this.btnRehacer.Click += new System.EventHandler(this.btnRehacer_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // btnCortar
            // 
            this.btnCortar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCortar.Image = global::Bloc_de_Notas.Properties.Resources.Cut_16x;
            this.btnCortar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCortar.Name = "btnCortar";
            this.btnCortar.Size = new System.Drawing.Size(23, 22);
            this.btnCortar.Text = "Cort&ar";
            this.btnCortar.Click += new System.EventHandler(this.menuBtnCortar_Click);
            // 
            // btnCopiar
            // 
            this.btnCopiar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopiar.Image = global::Bloc_de_Notas.Properties.Resources.Copy_16x;
            this.btnCopiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopiar.Name = "btnCopiar";
            this.btnCopiar.Size = new System.Drawing.Size(23, 22);
            this.btnCopiar.Text = "&Copiar";
            this.btnCopiar.Click += new System.EventHandler(this.menuBtnCopiar_Click);
            // 
            // btnPegar
            // 
            this.btnPegar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPegar.Image = global::Bloc_de_Notas.Properties.Resources.Paste_16x;
            this.btnPegar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPegar.Name = "btnPegar";
            this.btnPegar.Size = new System.Drawing.Size(23, 22);
            this.btnPegar.Text = "&Pegar";
            this.btnPegar.Click += new System.EventHandler(this.menuBtnPegar_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // btnFuente
            // 
            this.btnFuente.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFuente.Image = global::Bloc_de_Notas.Properties.Resources.FontColor_16x;
            this.btnFuente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFuente.Name = "btnFuente";
            this.btnFuente.Size = new System.Drawing.Size(23, 22);
            this.btnFuente.Text = "Fuente...";
            this.btnFuente.Click += new System.EventHandler(this.btnFuente_Click);
            // 
            // btnColor
            // 
            this.btnColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnColor.Image = global::Bloc_de_Notas.Properties.Resources.ColorWheel_16x;
            this.btnColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(23, 22);
            this.btnColor.Text = "Color...";
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // btnBuscar
            // 
            this.btnBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBuscar.Image = global::Bloc_de_Notas.Properties.Resources.Search_16x;
            this.btnBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(23, 22);
            this.btnBuscar.Text = "Buscar...";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnBuscarReemplazar
            // 
            this.btnBuscarReemplazar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBuscarReemplazar.Image = global::Bloc_de_Notas.Properties.Resources.ReplaceAll_16x;
            this.btnBuscarReemplazar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBuscarReemplazar.Name = "btnBuscarReemplazar";
            this.btnBuscarReemplazar.Size = new System.Drawing.Size(23, 22);
            this.btnBuscarReemplazar.Text = "Buscar y reemplazar...";
            this.btnBuscarReemplazar.Click += new System.EventHandler(this.btnBuscarReemplazar_Click);
            // 
            // barraEstado_inferior
            // 
            this.barraEstado_inferior.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.status_label_posicion,
            this.label_alineacionDerecha,
            this.toolStripStatusLabel1,
            this.label_Zoom,
            this.label_NumeroCoincidenciasBusqueda});
            this.barraEstado_inferior.Location = new System.Drawing.Point(0, 463);
            this.barraEstado_inferior.Name = "barraEstado_inferior";
            this.barraEstado_inferior.Padding = new System.Windows.Forms.Padding(1, 0, 15, 0);
            this.barraEstado_inferior.Size = new System.Drawing.Size(801, 22);
            this.barraEstado_inferior.TabIndex = 2;
            this.barraEstado_inferior.Text = "statusStrip1";
            // 
            // status_label_posicion
            // 
            this.status_label_posicion.Name = "status_label_posicion";
            this.status_label_posicion.Size = new System.Drawing.Size(106, 17);
            this.status_label_posicion.Text = "Línea 1, columna 1";
            // 
            // label_alineacionDerecha
            // 
            this.label_alineacionDerecha.Name = "label_alineacionDerecha";
            this.label_alineacionDerecha.Size = new System.Drawing.Size(584, 17);
            this.label_alineacionDerecha.Spring = true;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // label_Zoom
            // 
            this.label_Zoom.Name = "label_Zoom";
            this.label_Zoom.Size = new System.Drawing.Size(83, 17);
            this.label_Zoom.Text = "Zoom: normal";
            // 
            // label_NumeroCoincidenciasBusqueda
            // 
            this.label_NumeroCoincidenciasBusqueda.Name = "label_NumeroCoincidenciasBusqueda";
            this.label_NumeroCoincidenciasBusqueda.Size = new System.Drawing.Size(12, 17);
            this.label_NumeroCoincidenciasBusqueda.Text = "-";
            // 
            // menu_clickDerecho
            // 
            this.menu_clickDerecho.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextMenu_item_Cortar,
            this.contextMenu_item_Copiar,
            this.contextMenu_item_Pegar,
            this.toolStripSeparator8,
            this.contextMenuFuente});
            this.menu_clickDerecho.Name = "contextMenu_accionesSobreTexto";
            this.menu_clickDerecho.Size = new System.Drawing.Size(161, 98);
            // 
            // contextMenu_item_Cortar
            // 
            this.contextMenu_item_Cortar.Image = global::Bloc_de_Notas.Properties.Resources.Cut_16x;
            this.contextMenu_item_Cortar.Name = "contextMenu_item_Cortar";
            this.contextMenu_item_Cortar.Size = new System.Drawing.Size(160, 22);
            this.contextMenu_item_Cortar.Text = "Cortar";
            this.contextMenu_item_Cortar.Click += new System.EventHandler(this.menuContextualCortar_Click);
            // 
            // contextMenu_item_Copiar
            // 
            this.contextMenu_item_Copiar.Image = global::Bloc_de_Notas.Properties.Resources.Copy_16x;
            this.contextMenu_item_Copiar.Name = "contextMenu_item_Copiar";
            this.contextMenu_item_Copiar.Size = new System.Drawing.Size(160, 22);
            this.contextMenu_item_Copiar.Text = "Copiar";
            this.contextMenu_item_Copiar.Click += new System.EventHandler(this.menuContextualCopiar_Click);
            // 
            // contextMenu_item_Pegar
            // 
            this.contextMenu_item_Pegar.Image = global::Bloc_de_Notas.Properties.Resources.Paste_16x;
            this.contextMenu_item_Pegar.Name = "contextMenu_item_Pegar";
            this.contextMenu_item_Pegar.Size = new System.Drawing.Size(160, 22);
            this.contextMenu_item_Pegar.Text = "Pegar";
            this.contextMenu_item_Pegar.Click += new System.EventHandler(this.menuContextualPegar_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(157, 6);
            // 
            // contextMenuFuente
            // 
            this.contextMenuFuente.Image = global::Bloc_de_Notas.Properties.Resources.FontColor_16x;
            this.contextMenuFuente.Name = "contextMenuFuente";
            this.contextMenuFuente.Size = new System.Drawing.Size(160, 22);
            this.contextMenuFuente.Text = "Fuente y Color...";
            this.contextMenuFuente.Click += new System.EventHandler(this.contextMenuFuente_Click);
            // 
            // layoutTablaPrincipal
            // 
            this.layoutTablaPrincipal.AllowDrop = true;
            this.layoutTablaPrincipal.ColumnCount = 1;
            this.layoutTablaPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutTablaPrincipal.Controls.Add(this.inputCuadroTexto, 0, 0);
            this.layoutTablaPrincipal.Controls.Add(this.panelBuscarReemplazar, 0, 1);
            this.layoutTablaPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutTablaPrincipal.Location = new System.Drawing.Point(0, 49);
            this.layoutTablaPrincipal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutTablaPrincipal.Name = "layoutTablaPrincipal";
            this.layoutTablaPrincipal.RowCount = 2;
            this.layoutTablaPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75.60972F));
            this.layoutTablaPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.39028F));
            this.layoutTablaPrincipal.Size = new System.Drawing.Size(801, 414);
            this.layoutTablaPrincipal.TabIndex = 3;
            this.layoutTablaPrincipal.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.layoutTablaPrincipal_CellPaint);
            // 
            // inputCuadroTexto
            // 
            this.inputCuadroTexto.AcceptsTab = true;
            this.inputCuadroTexto.AllowDrop = true;
            this.inputCuadroTexto.ContextMenuStrip = this.menu_clickDerecho;
            this.inputCuadroTexto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputCuadroTexto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputCuadroTexto.Location = new System.Drawing.Point(3, 2);
            this.inputCuadroTexto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputCuadroTexto.Name = "inputCuadroTexto";
            this.inputCuadroTexto.Size = new System.Drawing.Size(795, 309);
            this.inputCuadroTexto.TabIndex = 5;
            this.inputCuadroTexto.Text = "";
            this.inputCuadroTexto.DragDrop += new System.Windows.Forms.DragEventHandler(this.inputCuadroTexto_DragDrop);
            this.inputCuadroTexto.SelectionChanged += new System.EventHandler(this.inputCuadroTexto_SelectionChanged);
            this.inputCuadroTexto.TextChanged += new System.EventHandler(this.inputCuadroTexto_TextChanged);
            this.inputCuadroTexto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputCuadroTexto_KeyDown);
            // 
            // panelBuscarReemplazar
            // 
            this.panelBuscarReemplazar.AllowDrop = true;
            this.panelBuscarReemplazar.AutoSize = true;
            this.panelBuscarReemplazar.ColumnCount = 4;
            this.panelBuscarReemplazar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.84058F));
            this.panelBuscarReemplazar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.15942F));
            this.panelBuscarReemplazar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.panelBuscarReemplazar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.panelBuscarReemplazar.Controls.Add(this.btnReemplazarSiguiente, 2, 2);
            this.panelBuscarReemplazar.Controls.Add(this.labelBuscar, 0, 1);
            this.panelBuscarReemplazar.Controls.Add(this.labelReemplazar, 0, 2);
            this.panelBuscarReemplazar.Controls.Add(this.inputBuscar, 1, 1);
            this.panelBuscarReemplazar.Controls.Add(this.btnBuscarTodos, 2, 1);
            this.panelBuscarReemplazar.Controls.Add(this.inputReemplazar, 1, 2);
            this.panelBuscarReemplazar.Controls.Add(this.lblCierraBuscarReemplazar, 3, 0);
            this.panelBuscarReemplazar.Controls.Add(this.btnReemplazarTodos, 3, 2);
            this.panelBuscarReemplazar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBuscarReemplazar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.panelBuscarReemplazar.Location = new System.Drawing.Point(3, 315);
            this.panelBuscarReemplazar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelBuscarReemplazar.Name = "panelBuscarReemplazar";
            this.panelBuscarReemplazar.RowCount = 3;
            this.panelBuscarReemplazar.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.panelBuscarReemplazar.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.panelBuscarReemplazar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelBuscarReemplazar.Size = new System.Drawing.Size(795, 97);
            this.panelBuscarReemplazar.TabIndex = 4;
            // 
            // btnReemplazarSiguiente
            // 
            this.btnReemplazarSiguiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReemplazarSiguiente.AutoSize = true;
            this.btnReemplazarSiguiente.Location = new System.Drawing.Point(502, 58);
            this.btnReemplazarSiguiente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReemplazarSiguiente.Name = "btnReemplazarSiguiente";
            this.btnReemplazarSiguiente.Size = new System.Drawing.Size(132, 25);
            this.btnReemplazarSiguiente.TabIndex = 6;
            this.btnReemplazarSiguiente.Text = "Reemplazar siguiente";
            this.btnReemplazarSiguiente.UseVisualStyleBackColor = true;
            this.btnReemplazarSiguiente.Click += new System.EventHandler(this.btnReemplazarSiguiente_Click);
            // 
            // labelBuscar
            // 
            this.labelBuscar.AutoSize = true;
            this.labelBuscar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBuscar.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBuscar.Location = new System.Drawing.Point(3, 15);
            this.labelBuscar.Name = "labelBuscar";
            this.labelBuscar.Size = new System.Drawing.Size(88, 29);
            this.labelBuscar.TabIndex = 0;
            this.labelBuscar.Text = "Buscar:";
            this.labelBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelReemplazar
            // 
            this.labelReemplazar.AutoSize = true;
            this.labelReemplazar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelReemplazar.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReemplazar.Location = new System.Drawing.Point(3, 44);
            this.labelReemplazar.Name = "labelReemplazar";
            this.labelReemplazar.Size = new System.Drawing.Size(88, 53);
            this.labelReemplazar.TabIndex = 3;
            this.labelReemplazar.Text = "Reemplazar:";
            this.labelReemplazar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // inputBuscar
            // 
            this.inputBuscar.AllowDrop = true;
            this.inputBuscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.inputBuscar.Location = new System.Drawing.Point(97, 19);
            this.inputBuscar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputBuscar.Name = "inputBuscar";
            this.inputBuscar.Size = new System.Drawing.Size(399, 20);
            this.inputBuscar.TabIndex = 1;
            this.inputBuscar.TextChanged += new System.EventHandler(this.inputBuscar_TextChanged);
            // 
            // btnBuscarTodos
            // 
            this.btnBuscarTodos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBuscarTodos.AutoSize = true;
            this.panelBuscarReemplazar.SetColumnSpan(this.btnBuscarTodos, 2);
            this.btnBuscarTodos.Location = new System.Drawing.Point(502, 17);
            this.btnBuscarTodos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBuscarTodos.Name = "btnBuscarTodos";
            this.btnBuscarTodos.Size = new System.Drawing.Size(290, 25);
            this.btnBuscarTodos.TabIndex = 4;
            this.btnBuscarTodos.Text = "Buscar todos";
            this.btnBuscarTodos.UseVisualStyleBackColor = true;
            this.btnBuscarTodos.Click += new System.EventHandler(this.btnBuscarTodos_Click);
            // 
            // inputReemplazar
            // 
            this.inputReemplazar.AllowDrop = true;
            this.inputReemplazar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.inputReemplazar.Location = new System.Drawing.Point(97, 60);
            this.inputReemplazar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputReemplazar.Name = "inputReemplazar";
            this.inputReemplazar.Size = new System.Drawing.Size(399, 20);
            this.inputReemplazar.TabIndex = 2;
            this.inputReemplazar.TextChanged += new System.EventHandler(this.inputReemplazar_TextChanged);
            this.inputReemplazar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputReemplazar_KeyDown);
            // 
            // lblCierraBuscarReemplazar
            // 
            this.lblCierraBuscarReemplazar.AutoSize = true;
            this.lblCierraBuscarReemplazar.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCierraBuscarReemplazar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCierraBuscarReemplazar.Location = new System.Drawing.Point(779, 0);
            this.lblCierraBuscarReemplazar.Name = "lblCierraBuscarReemplazar";
            this.lblCierraBuscarReemplazar.Size = new System.Drawing.Size(13, 15);
            this.lblCierraBuscarReemplazar.TabIndex = 10;
            this.lblCierraBuscarReemplazar.Text = "x";
            this.lblCierraBuscarReemplazar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCierraBuscarReemplazar.Click += new System.EventHandler(this.lblCierraBuscarReemplazar_Click);
            // 
            // btnReemplazarTodos
            // 
            this.btnReemplazarTodos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReemplazarTodos.AutoSize = true;
            this.btnReemplazarTodos.Location = new System.Drawing.Point(640, 58);
            this.btnReemplazarTodos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReemplazarTodos.Name = "btnReemplazarTodos";
            this.btnReemplazarTodos.Size = new System.Drawing.Size(152, 25);
            this.btnReemplazarTodos.TabIndex = 8;
            this.btnReemplazarTodos.Text = "Reemplazar todos";
            this.btnReemplazarTodos.UseVisualStyleBackColor = true;
            this.btnReemplazarTodos.Click += new System.EventHandler(this.btnReemplazarTodos_Click);
            // 
            // VentanaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(801, 485);
            this.Controls.Add(this.layoutTablaPrincipal);
            this.Controls.Add(this.barraEstado_inferior);
            this.Controls.Add(this.barraHerramientas);
            this.Controls.Add(this.barraMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.barraMenu;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(606, 288);
            this.Name = "VentanaPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bloc de Notas - by Luis Miguel Soto Sánchez";
            this.Load += new System.EventHandler(this.VentanaPrincipal_Load);
            this.barraMenu.ResumeLayout(false);
            this.barraMenu.PerformLayout();
            this.barraHerramientas.ResumeLayout(false);
            this.barraHerramientas.PerformLayout();
            this.barraEstado_inferior.ResumeLayout(false);
            this.barraEstado_inferior.PerformLayout();
            this.menu_clickDerecho.ResumeLayout(false);
            this.layoutTablaPrincipal.ResumeLayout(false);
            this.layoutTablaPrincipal.PerformLayout();
            this.panelBuscarReemplazar.ResumeLayout(false);
            this.panelBuscarReemplazar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip barraMenu;
        private System.Windows.Forms.ToolStripMenuItem toolstripmenu_item_Archivo;
        private System.Windows.Forms.ToolStripMenuItem menuBtnNuevo;
        private System.Windows.Forms.ToolStripMenuItem menuBtnAbrir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem menuBtnGuardar;
        private System.Windows.Forms.ToolStripMenuItem menuBtnGuardarComo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolstripmenu_item_Editar;
        private System.Windows.Forms.ToolStripMenuItem menuBtnDeshacer;
        private System.Windows.Forms.ToolStripMenuItem menuBtnRehacer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menuBtnCortar;
        private System.Windows.Forms.ToolStripMenuItem menuBtnCopiar;
        private System.Windows.Forms.ToolStripMenuItem menuBtnPegar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStrip barraHerramientas;
        private System.Windows.Forms.StatusStrip barraEstado_inferior;
        private System.Windows.Forms.ToolStripMenuItem menuBtnSalir;
        private System.Windows.Forms.ToolStripMenuItem menuBtnBuscar;
        private System.Windows.Forms.ToolStripMenuItem menuBtnFuente;
        private System.Windows.Forms.ToolStripMenuItem menuBtnColor;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnAbrir;
        private System.Windows.Forms.ToolStripButton btnGuardar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnCortar;
        private System.Windows.Forms.ToolStripButton btnCopiar;
        private System.Windows.Forms.ToolStripButton btnPegar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem menuBtnBuscarReemplazar;
        private System.Windows.Forms.ToolStripButton btnGuardarComo;
        private System.Windows.Forms.ToolStripButton btnRehacer;
        private System.Windows.Forms.ToolStripButton btnDeshacer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton btnFuente;
        private System.Windows.Forms.ToolStripButton btnColor;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton btnBuscar;
        private System.Windows.Forms.ToolStripStatusLabel status_label_posicion;
        private System.Windows.Forms.ToolStripButton btnBuscarReemplazar;
        private System.Windows.Forms.ContextMenuStrip menu_clickDerecho;
        private System.Windows.Forms.ToolStripMenuItem contextMenu_item_Cortar;
        private System.Windows.Forms.ToolStripMenuItem contextMenu_item_Copiar;
        private System.Windows.Forms.ToolStripMenuItem contextMenu_item_Pegar;
        private System.Windows.Forms.FontDialog dialog_fuente;
        private System.Windows.Forms.ToolStripStatusLabel label_alineacionDerecha;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private ColorDialog dialog_color;
        private TableLayoutPanel layoutTablaPrincipal;
        private RichTextBox inputCuadroTexto;
        private TableLayoutPanel panelBuscarReemplazar;
        private Label labelBuscar;
        private TextBox inputBuscar;
        private TextBox inputReemplazar;
        private Label labelReemplazar;
        private Button btnBuscarTodos;
        private Button btnReemplazarSiguiente;
        private Button btnReemplazarTodos;
        private Label lblCierraBuscarReemplazar;
        private ToolStripStatusLabel label_NumeroCoincidenciasBusqueda;
        private ToolStripSeparator toolStripSeparator8;
        private ToolStripMenuItem contextMenuFuente;
        private ToolStripStatusLabel label_Zoom;
        private ToolStripMenuItem menuBtnSoloBuscar;
    }
}

