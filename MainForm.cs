﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Bloc_de_Notas
{
    public partial class VentanaPrincipal : Form
    {
        //
        // Constantes
        //
        public const string TITULO_VENTANA = "Bloc de Notas - by Luis Miguel Soto Sánchez";

        //
        // Atributos
        //
        public GestorArchivos gestorArchivos;

        public VentanaPrincipal()
        {
            InitializeComponent();

            // Inicializar variables
            gestorArchivos = new GestorArchivos();

        }

        //
        // Otros métodos
        //
        private void limpiarTexto()
        {
            inputCuadroTexto.Clear();
            gestorArchivos.setArchivoGuardado(false);
        }
        public void actualizarTituloVentana()
        {

            if (gestorArchivos.isArchivoGuardado())
                this.Text = Path.GetFileNameWithoutExtension(gestorArchivos.getUltimoArchivoProcesado()) + " - " + TITULO_VENTANA;
            else if (gestorArchivos.hayArchivosProcesados())
                this.Text = Path.GetFileNameWithoutExtension(gestorArchivos.getUltimoArchivoProcesado()) + "* - " + TITULO_VENTANA;
            else
                this.Text = GestorArchivos.NOMBRE_ARCHIVO_DEFECTO + "* - " + TITULO_VENTANA;
        }

        private void toggleBuscarReemplazar(object sender)
        {
            // Distinguir quién lo ha llamado
            bool buscar = sender.ToString().Equals(menuBtnBuscar.Text) ||
                          sender.ToString().Equals(btnBuscar.Text) ||
                          sender.ToString().Equals(menuBtnSoloBuscar.Text) ? true : false;

            bool reemplazar = sender.ToString().Equals(btnBuscarReemplazar.Text) ?
                              true : false;

            // Establecer Foco en el InputBuscar
            this.ActiveControl = inputBuscar;

            // Comprobar si ha sido llamado por el botón de buscar o por el de reemplazar
            if (buscar)
            {
                // Mostrar panel entero de Buscar y Reemplazar
                layoutTablaPrincipal.RowStyles[1].SizeType = SizeType.AutoSize;

                // Ocultar panel de Reemplazar
                panelBuscarReemplazar.RowStyles[2].Height = 0;
                panelBuscarReemplazar.RowStyles[2].SizeType = SizeType.Absolute;

            }
            else if (reemplazar)
            {

                // Mostrar panel entero de Buscar y Reemplazar
                layoutTablaPrincipal.RowStyles[1].SizeType = SizeType.AutoSize;

                // Mostrar panel de Reemplazar
                panelBuscarReemplazar.RowStyles[2].Height = panelBuscarReemplazar.RowStyles[0].Height;
                panelBuscarReemplazar.RowStyles[2].SizeType = SizeType.AutoSize;
            }
        }

        //
        // Eventos
        //

        // Pintar Borde de segunda fila
        private void VentanaPrincipal_Load(object sender, EventArgs e)
        {
            // Establecer el foco en el richtextbox cuando la aplicación inicie
            inputCuadroTexto.Select();

            // Ocultar tabla inicialmente
            layoutTablaPrincipal.RowStyles[1].Height = 0;

            // Pintar tabla
            layoutTablaPrincipal.CellPaint += new TableLayoutCellPaintEventHandler(layoutTablaPrincipal_CellPaint);
        }
        private void layoutTablaPrincipal_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Row == 1)
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(158, 158, 158)), e.CellBounds);
        }

        private void herramientasFuente_Click(object sender, EventArgs e)
        {
            dialog_fuente.ShowColor = false;

            if (dialog_fuente.ShowDialog() != DialogResult.Cancel)
            {
                if (!string.IsNullOrEmpty(inputCuadroTexto.SelectedText))
                    inputCuadroTexto.SelectionFont = dialog_fuente.Font;
                else
                    inputCuadroTexto.Font = dialog_fuente.Font;
            }

        }

        private void menuBtnSalir_Click(object sender, EventArgs e)
        {
            if (!gestorArchivos.isArchivoGuardado())
                menuBtnGuardar_Click(sender, e);
            
            Application.Exit();
        }

        private void inputCuadroTexto_SelectionChanged(object sender, EventArgs e)
        {
            int linea = inputCuadroTexto.GetLineFromCharIndex(inputCuadroTexto.SelectionStart) + 1;
            int columna = inputCuadroTexto.SelectionStart - inputCuadroTexto.GetFirstCharIndexOfCurrentLine() + 1;

            status_label_posicion.Text = "Línea " + linea.ToString() + ", columna " + columna.ToString();
        }

        private void menuBtnNuevo_Click(object sender, EventArgs e)
        {
            DialogResult guardarArchivo;

            // Comprobar que el RichTextBox no esté vacío
            if (!inputCuadroTexto.Text.Trim().Equals(string.Empty))
            {
                guardarArchivo = MessageBox.Show("¿Desea guardar el archivo actual?", "Crear nuevo archivo", MessageBoxButtons.YesNo);

                if (guardarArchivo == DialogResult.Yes)
                {

                    // Dialogo Guardar Archivo
                    SaveFileDialog dialogo = gestorArchivos.GetDialogoGuardar();


                    if (dialogo.ShowDialog() == DialogResult.OK)
                    {
                        // Guardar archivo

                        // TXT
                        if (Path.GetExtension(dialogo.FileName).ToLower().Equals(".txt"))
                            File.WriteAllText(dialogo.FileName, inputCuadroTexto.Text.Replace("\n", "\r\n"));
                        else //RTF
                            inputCuadroTexto.SaveFile(dialogo.FileName, RichTextBoxStreamType.RichText);

                        gestorArchivos.anadirArchivo(dialogo.FileName);
                        gestorArchivos.setArchivoGuardado(true);

                    }
                }
                else
                    limpiarTexto();
            }
        }

        private void menuBtnAbrir_Click(object sender, EventArgs e)
        {
            // Abrir diálogo de selección de archivo
            OpenFileDialog dialogo = gestorArchivos.GetDialogoElegirArchivo();

            // Permitir seleccionar archivo
            if (dialogo.ShowDialog() == DialogResult.OK) {

                gestorArchivos.anadirArchivo(dialogo.FileName);

                // Leer archivo
                try {
                    gestorArchivos.AbrirArchivo(inputCuadroTexto, dialogo.FileName);
                    actualizarTituloVentana();

                }
                catch (IOException)
                {
                    MessageBox.Show("Ha ocurrido un error intentando leer el archivo situado en:\n " + dialogo.FileName, "Error de lectura", MessageBoxButtons.OK);

                }
                catch (ArgumentException)
                {
                    MessageBox.Show("El formato del archivo en \"\n " + dialogo.FileName + "\" no es válido.", "Error de archivo", MessageBoxButtons.OK);
                }

                actualizarTituloVentana();
            }
        }

        private void menuBtnGuardar_Click(object sender, EventArgs e)
        {
            string ultArchivoProcesado = gestorArchivos.getUltimoArchivoProcesado();

            // Comprobar si el archvo ha sido guardado con anterioridad
            if (!ultArchivoProcesado.Equals(string.Empty))
            {
                // Obtener el archivo
                if (File.Exists(ultArchivoProcesado))
                {

                    // Comprobar extension
                    if (Path.GetExtension(ultArchivoProcesado).ToLower().Equals(".txt"))
                        File.WriteAllText(ultArchivoProcesado, inputCuadroTexto.Text.Replace("\n", "\r\n")); // TXT
                    else
                        inputCuadroTexto.SaveFile(ultArchivoProcesado, RichTextBoxStreamType.RichText); // RTF

                    // Indicar archivo como guardado
                    gestorArchivos.setArchivoGuardado(true);

                    actualizarTituloVentana();
                }
                    
            }

            // El archivo no existe, llamar a Guardar Como...
            menuBtnGuardarComo_Click(sender, e);

        }

        private void menuBtnGuardarComo_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialogo = gestorArchivos.GetDialogoGuardarComo();

            // Mostrar diálogo de guardado
            if (dialogo.ShowDialog() == DialogResult.OK)
            {
                if (Path.GetExtension(dialogo.FileName).ToLower().Equals(".txt"))
                {
                    // Crear archivo nuevo
                    File.WriteAllText(dialogo.FileName, inputCuadroTexto.Text.Replace("\n", "\r\n")); // TXT
                }
                else
                {
                    inputCuadroTexto.SaveFile(dialogo.FileName, RichTextBoxStreamType.RichText); // RTF
                }

                // Guardar datos del archivo
                gestorArchivos.anadirArchivo(dialogo.FileName);

                gestorArchivos.setArchivoGuardado(true);
                actualizarTituloVentana();

            }
        }

        private void inputCuadroTexto_TextChanged(object sender, EventArgs e)
        {
            gestorArchivos.setArchivoGuardado(false);
            actualizarTituloVentana();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            menuBtnNuevo_Click(sender, e);
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            menuBtnAbrir_Click(sender, e);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            menuBtnGuardar_Click(sender, e);
        }

        private void btnGuardarComo_Click(object sender, EventArgs e)
        {
            menuBtnGuardarComo_Click(sender, e);
        }

        private void inputCuadroTexto_DragDrop(object sender, DragEventArgs e)
        {
            // Comprobar tipo de archivo
            object archivosSoltados = e.Data.GetData("FileDrop");

            if (archivosSoltados != null)
            {
                string[] listaArchivos = archivosSoltados as string[];

                if (listaArchivos != null && !string.IsNullOrWhiteSpace(listaArchivos[0]))
                {
                    gestorArchivos.anadirArchivo(listaArchivos[0]);
                    gestorArchivos.AbrirArchivo(inputCuadroTexto, listaArchivos[0]);

                    actualizarTituloVentana();
                }
            }
        }

        private void menuBtnDeshacer_Click(object sender, EventArgs e)
        {
            inputCuadroTexto.Undo();
        }

        private void btnDeshacer_Click(object sender, EventArgs e)
        {
            menuBtnDeshacer_Click(sender, e);
        }
        private void menuBtnRehacer_Click(object sender, EventArgs e)
        {
            inputCuadroTexto.Redo();
        }

        private void btnRehacer_Click(object sender, EventArgs e)
        {
            menuBtnRehacer_Click(sender, e);
        }

        private void menuBtnCortar_Click(object sender, EventArgs e)
        {
            inputCuadroTexto.Cut();
        }

        private void menuBtnCopiar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(inputCuadroTexto.SelectedText))
                inputCuadroTexto.Copy();
            else
            {
                inputCuadroTexto.SelectAll();
                inputCuadroTexto.Copy();
                inputCuadroTexto.DeselectAll();
            }
        }

        private void menuBtnPegar_Click(object sender, EventArgs e)
        {
            inputCuadroTexto.Paste();
        }

        private void menuContextualCortar_Click(object sender, EventArgs e)
        {
            menuBtnCortar_Click(sender, e);
        }

        private void menuContextualCopiar_Click(object sender, EventArgs e)
        {
            menuBtnCopiar_Click(sender, e);
        }

        private void menuContextualPegar_Click(object sender, EventArgs e)
        {
            menuBtnPegar_Click(sender, e);
        }

        private void btnFuente_Click(object sender, EventArgs e)
        {
            herramientasFuente_Click(sender, e);
        }

        private void menuBtnColor_Click(object sender, EventArgs e)
        {
            if (dialog_color.ShowDialog() != DialogResult.Cancel)
            {
                if (!string.IsNullOrEmpty(inputCuadroTexto.SelectedText))
                    inputCuadroTexto.SelectionColor = dialog_color.Color;

            }
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            menuBtnColor_Click(sender, e);
        }

        private void menuBtnBuscar_Click(object sender, EventArgs e)
        {
            toggleBuscarReemplazar(sender);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            menuBtnBuscar_Click(sender, e);
        }

        private void menuBtnBuscarReemplazar_Click(object sender, EventArgs e)
        {
            toggleBuscarReemplazar(sender);
        }

        private void btnBuscarReemplazar_Click(object sender, EventArgs e)
        {
            menuBtnBuscarReemplazar_Click(sender, e);
        }

        private void lblCierraBuscarReemplazar_Click(object sender, EventArgs e)
        {
            // Cerrar fila buscar y reemplazar
            layoutTablaPrincipal.RowStyles[1].SizeType = SizeType.Absolute;
        }

        private void btnReemplazarSiguiente_Click(object sender, EventArgs e)
        {
            inputCuadroTexto.Text = inputCuadroTexto.Text.ReemplazarPrimeraCoincidencia(inputBuscar.Text, inputReemplazar.Text);

            // Ir buscando a medida que el usuario escribe
            inputCuadroTexto.ResaltarTexto(inputBuscar.Text);
        }

        private void inputBuscar_TextChanged(object sender, EventArgs e)
        {
            // Ir buscando a medida que el usuario escribe
            inputCuadroTexto.ResaltarTexto(inputBuscar.Text);

            // Actualizar el número de coincidencias en la barra de estado inferior
            int numeroCoincidencias = inputCuadroTexto.Text.GetNumeroCoincidencias(inputBuscar.Text);

            if (numeroCoincidencias > 0)
                label_NumeroCoincidenciasBusqueda.Text = "Coincidencias: " + numeroCoincidencias;

            if (inputBuscar.Text.Equals(string.Empty))
                label_NumeroCoincidenciasBusqueda.Text = "-";
        }

        // Reemplaza todas las coincidencias encontradas en el Cuadro de Texto principal
        private void btnReemplazarTodos_Click(object sender, EventArgs e)
        {
            inputCuadroTexto.Text = inputCuadroTexto.Text.Replace(inputBuscar.Text, inputReemplazar.Text);
        }

        private void inputReemplazar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnReemplazarSiguiente_Click(sender, e);

        }

        private void inputReemplazar_TextChanged(object sender, EventArgs e)
        {
            // Ir buscando a medida que el usuario escribe
            inputCuadroTexto.ResaltarTexto(inputBuscar.Text);
        }

        private void btnBuscarTodos_Click(object sender, EventArgs e)
        {
            inputReemplazar_TextChanged(sender, e);
            MessageBox.Show("Ha/n habido " + Buscador.GetNumeroCoincidencias(inputCuadroTexto.Text, inputBuscar.Text) + " coincidencia/s.",
                            "Resultado búsqueda", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void contextMenuFuente_Click(object sender, EventArgs e)
        {
            dialog_fuente.ShowColor = true;

            if (dialog_fuente.ShowDialog() != DialogResult.Cancel)
            {
                if (!string.IsNullOrEmpty(inputCuadroTexto.SelectedText))
                    inputCuadroTexto.SelectionFont = dialog_fuente.Font;
                else
                    inputCuadroTexto.Font = dialog_fuente.Font;
            }
        }

        private void inputCuadroTexto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Add && inputCuadroTexto.ZoomFactor < 10)
                inputCuadroTexto.ZoomFactor = inputCuadroTexto.ZoomFactor * 2;

            else if (e.Control && e.KeyCode == Keys.Subtract && inputCuadroTexto.ZoomFactor > 1)
                inputCuadroTexto.ZoomFactor = inputCuadroTexto.ZoomFactor/2;

            // Actualizar Label de Zoom
            label_Zoom.Text = "Zoom: " + ((inputCuadroTexto.ZoomFactor > 1)? "x" + inputCuadroTexto.ZoomFactor: "normal");
        }

        private void menuBtnSoloBuscar_Click(object sender, EventArgs e)
        {
            menuBtnBuscar_Click(sender, e);
        }
    }
}